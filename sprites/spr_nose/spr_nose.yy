{
    "id": "3462ce54-6b8f-4285-993e-33d7159dddd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f42da626-387b-4fc2-bda2-d7e55edd520a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3462ce54-6b8f-4285-993e-33d7159dddd4",
            "compositeImage": {
                "id": "b11ad8b6-a823-4a35-bb76-6e3f12a4c9ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f42da626-387b-4fc2-bda2-d7e55edd520a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc27bcc-11fb-42c7-a4d1-a757ddd04867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f42da626-387b-4fc2-bda2-d7e55edd520a",
                    "LayerId": "55c472a5-586b-492d-9be9-e66a594c5466"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "55c472a5-586b-492d-9be9-e66a594c5466",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3462ce54-6b8f-4285-993e-33d7159dddd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}