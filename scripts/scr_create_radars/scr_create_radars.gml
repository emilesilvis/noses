///@description Create radars
///@arg nose

var nose = argument[0]

var radar_size = nose.radar_radius*2/3

var radar_1 = instance_create_layer(x, y, "Instances", obj_radar)
with(radar_1) {
	self.nose = nose
	self.size = radar_size
	self.position_in_nose = 1
}

var radar_2 = instance_create_layer(x, y, "Instances", obj_radar)
with(radar_2) {
	self.nose = nose
	self.size = radar_size
	self.position_in_nose = 2
}

var radar_3 = instance_create_layer(x, y, "Instances", obj_radar)
with(radar_3) {
	self.nose = nose
	self.size = radar_size
	self.position_in_nose = 3
}

var radar_4 = instance_create_layer(x, y, "Instances", obj_radar)
with(radar_4) {
	self.nose = nose
	self.size = radar_size
	self.position_in_nose = 4
}

var radar_5 = instance_create_layer(x, y, "Instances", obj_radar)
with(radar_5) {
	self.nose = nose
	self.size = radar_size
	self.position_in_nose = 5
}

var radar_6 = instance_create_layer(x, y, "Instances", obj_radar)
with(radar_6) {
	self.nose = nose
	self.size = radar_size
	self.position_in_nose = 6
}

var radar_7 = instance_create_layer(x, y, "Instances", obj_radar)
with(radar_7) {
	self.nose = nose
	self.size = radar_size
	self.position_in_nose = 7
}

var radar_8 = instance_create_layer(x, y, "Instances", obj_radar)
with(radar_8) {
	self.nose = nose
	self.size = radar_size
	self.position_in_nose = 8
}
