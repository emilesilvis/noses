{
    "id": "8438e71c-add6-4973-9001-78c540b8d934",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_nose",
    "eventList": [
        {
            "id": "99177fe5-a5a9-41ed-b948-51c468880249",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8438e71c-add6-4973-9001-78c540b8d934"
        },
        {
            "id": "3ed987fa-26f9-4b33-bd98-6ade193958e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8438e71c-add6-4973-9001-78c540b8d934"
        },
        {
            "id": "978ec512-22d3-46df-940b-ac20833853ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8438e71c-add6-4973-9001-78c540b8d934"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3462ce54-6b8f-4285-993e-33d7159dddd4",
    "visible": true
}