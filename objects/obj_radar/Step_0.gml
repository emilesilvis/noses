#region radar position

// TODO: Transform to case statement
if (position_in_nose == 1) {
	x = nose.x - nose.radar_radius
	y = nose.y - nose.radar_radius
} else if (position_in_nose == 2) {
	x = nose.x - nose.radar_radius + size
	y = nose.y - nose.radar_radius
} else if (position_in_nose == 3) {
	x = nose.x - nose.radar_radius + size * 2
	y = nose.y - nose.radar_radius
} else if (position_in_nose == 4) {
	x = nose.x - nose.radar_radius
	y = nose.y - nose.radar_radius + size
} else if (position_in_nose == 5)  {
	x = nose.x - nose.radar_radius + size * 2
	y = nose.y - nose.radar_radius + size
} else if (position_in_nose == 6)  {
	x = nose.x - nose.radar_radius
	y = nose.y - nose.radar_radius + size * 2
} else if (position_in_nose == 7)  {
	x = nose.x - nose.radar_radius + size
	y = nose.y - nose.radar_radius + size * 2
} else if (position_in_nose == 8)  {
	x = nose.x - nose.radar_radius + size * 2
	y = nose.y - nose.radar_radius + size * 2
}

#endregion 

#region atom counting

var atom_list = ds_list_create()

atom_count = collision_rectangle_list(x, y, x + size, y + size, obj_atom, false, true, atom_list, false);

ds_list_destroy(atom_list)

#endregion