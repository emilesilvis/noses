{
    "id": "a5ede43d-bf11-44fa-9223-97b346c4aa64",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_radar",
    "eventList": [
        {
            "id": "b1f5ae0c-3f71-408a-bae5-de0eb72d7eb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a5ede43d-bf11-44fa-9223-97b346c4aa64"
        },
        {
            "id": "53359699-0c03-4f62-9891-b2cee64af4fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a5ede43d-bf11-44fa-9223-97b346c4aa64"
        },
        {
            "id": "eec4aa7f-fb55-4adf-849b-7c0496e7b8d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a5ede43d-bf11-44fa-9223-97b346c4aa64"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}